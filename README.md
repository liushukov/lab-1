## Лабораторна робота 1

### Тема: "Git"

#### Виконав: Люшуков Кирило

### Результати тесту
#### Main
![Скріншот1](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo1.png)
![Скріншот2](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo2.png)
![Скріншот3](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo3.png)
![Скріншот4](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo4.png)
![Скріншот5](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo5.png)
![Скріншот6](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo6.png)
![Скріншот7](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo7.png)
![Скріншот8](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo8.png)
![Скріншот9](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo9.png)
![Скріншот10](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo10.png)
![Скріншот11](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo11.png)
![Скріншот12](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo12.png)
![Скріншот13](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo13.png)
![Скріншот14](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo14.png)
![Скріншот15](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo15.png)
![Скріншот16](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo16.png)
![Скріншот17](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo17.png)
![Скріншот18](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo18.png)
#### Результат Main
![Скріншот19](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo19.png)
---
#### Remote
![Скріншот20](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo20.png)
![Скріншот21](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo21.png)
![Скріншот22](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo22.png)
![Скріншот23](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo23.png)
![Скріншот24](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo24.png)
![Скріншот25](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo25.png)
![Скріншот26](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo26.png)
![Скріншот27](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo27.png)
![Скріншот28](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo28.png)
![Скріншот29](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo29.png)
![Скріншот30](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo30.png)
![Скріншот31](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo31.png)
![Скріншот32](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo32.png)
![Скріншот33](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo33.png)
![Скріншот34](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo34.png)
![Скріншот35](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo35.png)
#### Результат Remote
![Скріншот36](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo36.png)
---
### Практична частина

![Скріншот37](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo37.png)
**Перевірка версії гіта**
---
![Скріншот38](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo38.png)
**Ініціалізація директорії**
---
![Скріншот39](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo39.png)
**Задання ім’я та пошти користувача**
---
![Скріншот40](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo40.png)
**Додавання зв’язку з віддаленим репозиторієм**
---
![Скріншот41](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo41.png)
**Перший коміт і пуш**
---
![Скріншот42](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo42.png)
**Запушений проєкт на Гітлаб**
---
![Скріншот43](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo43.png)
![Скріншот44](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo44.png)
![Скріншот45](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo45.png)
![Скріншот46](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo46.png)
**Приклад роботи**
---
![Скріншот47](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo47.png)
**Пуш зробленого проєкту**
---
![Скріншот48](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo48.png)
**Запушений зроблений проєкт на Гітлаб**
---
![Скріншот49](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo49.png)
**Історія проєкту**
---
![Скріншот50](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo50.png)
**Додав сокомандника**
---
![Скріншот51](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo51.png)
**Merge request**
---
![Скріншот52](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo52.png)
**Додав сокомандника**
---
![Скріншот53](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo53.png)
**Merge request**
---
![Скріншот54](https://gitlab.com/liushukov/lab-1/-/raw/2eefdc3931651dd7cc173d5abac1fa05f4cf9a27/images/photo54.png)
**Історія проєкту**
---