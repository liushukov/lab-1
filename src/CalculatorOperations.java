public class CalculatorOperations implements Operations{
    @Override
    public double add(double a, double b) {
        return a + b;
    }

    @Override
    public double subtract(double a, double b) {
        return a - b;
    }

    @Override
    public double multiply(double a, double b) {
        return a * b;
    }

    @Override
    public double divide(double a, double b) throws ArithmeticException {
        if (b == 0){
            throw new ArithmeticException("You can't divide by zero!");
        }
        return a / b;
    }

    @Override
    public double[] quadraticRoot(double a, double b, double c) throws ArithmeticException {
        double discriminant = b * b - 4 * a * c;

        if (discriminant < 0) {
            throw new ArithmeticException("Error: Quadratic equation has no real roots.");
        }

        if (discriminant == 0) {
            double root = -b / (2 * a);
            return new double[]{root};
        }

        double root1 = (-b + Math.sqrt(discriminant)) / (2 * a);
        double root2 = (-b - Math.sqrt(discriminant)) / (2 * a);

        return new double[]{root1, root2};
    }
}
