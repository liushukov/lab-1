public interface Operations {
    double add(double a, double b);
    double subtract(double a, double b);
    double multiply(double a, double b);
    double divide(double a, double b) throws ArithmeticException;
    double[] quadraticRoot(double a, double b, double c) throws ArithmeticException;
}
