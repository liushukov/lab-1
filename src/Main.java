import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Operations calc = new CalculatorOperations();

        try (Scanner scanner = new Scanner(System.in)) {
            System.out.print("Choose digit: \n1) Add (a + b)\n2) Subtract (a - b)\n3) Multiply (a * b)" +
                    "\n4) Divide (a / b)\n5) Quadratic root (ax^2 + bx + c = 0)\n6) Convert\n7) NOK\n8) NOD" +
                    " \nYour choice: ");
            int operation = scanner.nextInt();
            switch (operation) {
                case 1 -> System.out.print("Result: " + calc.add(new Main().inputNumberA(scanner),
                        new Main().inputNumberB(scanner)));
                case 2 -> System.out.print("Result: " + calc.subtract(new Main().inputNumberA(scanner),
                        new Main().inputNumberB(scanner)));
                case 3 -> System.out.print("Result: " + calc.multiply(new Main().inputNumberA(scanner),
                        new Main().inputNumberB(scanner)));
                case 4 -> System.out.print("Result: " + calc.divide(new Main().inputNumberA(scanner),
                        new Main().inputNumberB(scanner)));
                case 5 -> {
                    double[] roots = calc.quadraticRoot(new Main().inputNumberA(scanner), new Main().inputNumberB(scanner),
                            new Main().inputNumberC(scanner));
                    System.out.print((roots.length > 1) ? "x1: " + roots[0] + "\nx2: " + roots[1] : "x: " + roots[0]);
                }
                case 6 -> {
                    System.out.println("Введіть перше число:");
                    double num1 = scanner.nextDouble();
                    System.out.println("Введіть одиниці вимірювання (b, kb, mb):");
                    String unit = scanner.next().toLowerCase();

                    System.out.println("Введіть значення:");
                    double value = scanner.nextDouble();

                    double result;
                    switch (unit) {
                        case "b":
                            result = convertBytes(num1, value);
                            break;
                        case "kb":
                            result = convertKilobytes(num1, value);
                            break;
                        case "mb":
                            result = convertMegabytes(num1, value);
                            break;
                        default:
                            System.out.println("Невідомі одиниці вимірювання");
                            return;
                    }

                    System.out.println("Результат конвертації: " + result);
                }
                case 7 -> {
                    System.out.println("Введіть перше число:");
                    int num1 = scanner.nextInt();
                    System.out.println("Введіть друге число:");
                    int num2 = scanner.nextInt();
                    int gcd = findGCD(num1, num2);
                    System.out.println("НОД: " + gcd);
                }
                case 8 -> {
                    System.out.println("Введіть перше число:");
                    int num1 = scanner.nextInt();
                    System.out.println("Введіть друге число:");
                    int num2 = scanner.nextInt();
                    int lcm = findLCM(num1, num2);
                    System.out.println("НОК: " + lcm);
                }
                default -> {
                    System.out.println("Error: Invalid operation.");
                    return;
                }
            }


        } catch (Exception e) {
            System.out.println("Error: Invalid input. Please enter valid numbers and operation.");
        }
    }
    public double inputNumberA(Scanner sc){
        System.out.print("a: ");
        return sc.nextDouble();
    }
    public double inputNumberB(Scanner sc){
        System.out.print("b: ");
        return sc.nextDouble();
    }
    public double inputNumberC(Scanner sc){
        System.out.print("c: ");
        return sc.nextDouble();
    }
    private static double convertBytes(double num1, double value) {
        return num1 * value;
    }

    private static double convertKilobytes(double num1, double value) {
        return num1 * (value * 1024);
    }

    private static double convertMegabytes(double num1, double value) {
        return num1 * (value * 1024 * 1024);
    }
    public static int findGCD(int a, int b) {
        if (b == 0)
            return a;
        return findGCD(b, a % b);
    }

    // Функція для обчислення найменшого спільного кратного (НСК)
    public static int findLCM(int a, int b) {
        return (a * b) / findGCD(a, b);
    }
}
